# Interactiv Rainbow Solar System

## Rules

1. Planet N should orbit around the coordinates (0,0)
1. Planet N+1 has the coordinates (0,0) in the middle of Planet N
1. Planet N+1 has a ring in the color of Planet N

## Interaction

- Left click: Add Planet (max. 5)
- Right Click: Remove Planet (min. 1)
- KeyPress("r"): Recording frames

## Used Methods

- Sin/Cos-Function
- Recursion
- Relative coordinate system (Push-/PopMatrix())
- Recording Mode (Press "r")

![](animation.gif)
