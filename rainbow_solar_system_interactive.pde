float x=0.0,y=0.0;
float grad = 0;
float firstSize = 100;
float firstSpeed = 0.1;
float firstRadius = firstSize*2.5;
int transp = 255;
float sizeShrinkFactor = 0.8;
float radiusShrinkFactor = 0.8;

int planetMaxCount = 4;
int planetCounter = 0;



boolean recording = false;

// Farben
color red = color(255,0,0,transp);
color green = color(0,255,0,transp);
color blue = color(0,0,255,transp);
color[] colors = {
 #69AB2C,
 #69785B,
 #DFD851,
 #918AE2,
 #2C44AB,
};

// Pro Frame
float size;
float radius;
float speed;

void setup() {
  size(1000, 1000);
  background(0);
  //frameRate(80);
}


void draw() {
  // Primer
  fill(0,20);
  rect(0,0,width,height);
  
  initPlanetValues();
  
  // Draw planets
  addPlanet();
  
  // Record frames if needed
  recording();
}

void initPlanetValues(){
  // Inital values for the planets
  size = firstSize;
  radius = firstRadius;
  speed = firstSpeed;
  planetCounter = 0;
  
  // First coordinate origin
  x = width/2;
  y = height/2;
}
void recording(){
  if(recording){
    if(frameCount%4 == 0){
      saveFrame("images/image####.png");
    }
    fill(red);
  }else{
    fill(green);
  }  
  circle(width - 50,50,30);
}

void mousePressed() {
  if (mouseButton == LEFT) {
    if(planetMaxCount < 5){
      planetMaxCount++;
    }
  } else if (mouseButton == RIGHT) {
    if(planetMaxCount > 1){
      planetMaxCount--;
    }
  }
}

// Toggle recording
void keyPressed(){
  if(key == 'r' || key == 'R'){
    recording = !recording;
  }
}

void addPlanet(){
  if(++planetCounter>planetMaxCount){return;}
  pushMatrix();
  translate(x,y);
  radius *= radiusShrinkFactor;
  size *= sizeShrinkFactor;
  speed *= 2;
  x=map(cos(radians(frameCount*speed)),0,1,0,radius);
  y=map(sin(radians(frameCount*speed)),0,1,0,radius);
  fill(colors[planetCounter%colors.length]);
  ellipse(x,y,size,size);
  fill(colors[(planetCounter+1)%colors.length]);
  ellipse(x,y,size*0.8,size*0.8);
  addPlanet();
  popMatrix();
}
